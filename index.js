import { my_sum } from "./day-1/exercise-0.js"
import { my_display_alpha } from "./day-1/exercise-1.js"
import { my_display_alpha_reverse } from "./day-1/exercise-2.js"
import { my_alpha_number } from "./day-1/exercise-3.js"
import { my_size_alpha } from "./day-1/exercise-4.js"
import {my_array_alpha } from "./day-1/exercise-5.js"
import { my_length_array } from "./day-1/exercise-6.js"
import { my_is_posi_neg } from "./day-1/exercise-7.js"

/*
console.log(my_sum(2));
console.log(my_display_alpha());
console.log(my_display_alpha_reverse());
console.log(my_alpha_number(69));
console.log(typeof(my_alpha_number(69)));
console.log(my_size_alpha("titi toto tata"));
console.log(my_array_alpha("titi toto tata"));
console.log(my_length_array([1, 2, 3, 4, 5, 6]));
*/

console.log(my_is_posi_neg(69));
console.log(my_is_posi_neg(-69));
console.log(my_is_posi_neg(0));
console.log(my_is_posi_neg());