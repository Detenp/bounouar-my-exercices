export const my_length_array = (arr) => {
    let res = 0;
    for (let e of arr) {
        res += 1;
    }
    return res;
}