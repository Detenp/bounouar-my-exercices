import { assert } from 'chai';
import { my_sum } from "../day-1/exercise-0.js"
import { my_display_alpha } from "../day-1/exercise-1.js"
import { my_display_alpha_reverse } from "../day-1/exercise-2.js"
import { my_alpha_number } from "../day-1/exercise-3.js"
import { my_size_alpha } from "../day-1/exercise-4.js"
import { my_array_alpha } from "../day-1/exercise-5.js"
import { my_length_array } from "../day-1/exercise-6.js"
import { my_is_posi_neg } from "../day-1/exercise-7.js"

describe('Exercise-0', function () {
    it('should return 0 when no values in either one', function () {
        assert.equal(0, my_sum());
        assert.equal(0, my_sum(1));
    });
    it('should return 0 if not numbers', function () {
        assert.equal(0, my_sum("toto", "tata"));
        assert.equal(0, my_sum(undefined, "zizi"));
        assert.equal(0, my_sum(15, undefined));
        assert.equal(0, my_sum(undefined, 15));
    });
    it('good result', function() {
        assert.equal(4, my_sum(2, 2));
        assert.equal(-4, my_sum(-2, -2));
        assert.equal(1, my_sum(4, -3));
    });
});

describe('Exercise-1', function() {
    it('print', function() {
        assert.equal("abcdefghijklmnopqrstuvwxyz", my_display_alpha());
    });
});

describe('Exercise-2', function() {
    it ('print', function() {
        assert.equal("zyxwvutsrqponmlkjihgfedcba", my_display_alpha_reverse());
    });
})

describe('Exercise-3', function() {
    it ('convert', function() {
        assert.equal("69", my_alpha_number(69));
    });
    it ('type', function() {
        assert.equal("string", typeof(my_alpha_number(69)))
    });
})

describe('Exercise-4', function() {
    it ('Should return 0 if not a string', function() {
        assert.equal(0, my_size_alpha(69));
        assert.equal(0, my_size_alpha(undefined));
        assert.equal(0, my_size_alpha(true));
    });
    it ('Return number of letters', function() {
        assert.equal(0, my_size_alpha(""));
        assert.equal(0, my_size_alpha("1234"));
        assert.equal(5, my_size_alpha("totot"));
        assert.equal(5, my_size_alpha("5tot5ot5"));
    })
})

describe('Exercise-5', function() {
    it ('Should return empty if not a string', function() {
        assert.isEmpty(my_array_alpha(69));
        assert.isEmpty(my_array_alpha(undefined));
        assert.isEmpty(my_array_alpha(true));
    })
    it ('Return letters', function() {
        assert.isEmpty(my_array_alpha(""));
        assert.isEmpty(my_array_alpha("1234"));
        assert.deepEqual(['t', 'o', 't', 'o', 't'], my_array_alpha("totot"))
        assert.deepEqual(['t', 'o', 't', 'o', 't'], my_array_alpha("5tot5ot5"))
    })
})

describe('Exercise-6', function() {
    it ('Return length', function() {
        assert.equal(0, my_length_array([]))
        assert.equal(6, my_length_array([1, 2, 3, 4, 5, 6]))
    })
})

describe('Exercise-7', function() {
    it ('Return positive_negative', function() {
        assert.equal("POSITIF", my_is_posi_neg(69));
        assert.equal("NEGATIF", my_is_posi_neg(-69));
        assert.equal("NEUTRAL", my_is_posi_neg(0));
        assert.equal("POSITIF", my_is_posi_neg());
    })
})